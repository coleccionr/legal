<div align="center">
![\\\\cR colección Rorikon](https://gitlab.com/coleccionr/brand/raw/master/Resources/Logotype/Bitmaps/logotype.png)
</div>

***
>>>
`This repository is currently under development, so it is likely that its content is incomplete or inconsistent, review the changelog file to be aware of the new contributions.`
>>>         


 # Legal Resources #
Here are all documents related to the legal information concerning the activity and use of colección Rorikon.

## Content of this respitory
In this repository are stored a copy of each of the published versions of the Terms and Conditions, as well as the Regulations applicable to the application and the services offered by the colecióón Rorikon.

## Changelog

If you have cloned this repository before or have a downloaded copy, do not forget to check the [changelog](CHANGELOG.md) to make sure you 
are working with the _latest changes uploaded_ to the repository

## Support

If you have any doubts or suggestions when using the elements provided in this repository, do not hesitate to contact me 
through the different means and networks available below, I will be attentive to answer any questions in the shortest 
possible time

[www.coleccionr.xyz](https://www.coleccionr.xyz)  

[support@coleccionr.xyz](mailto:support@coleccionr.xyz)  

[(+57)305 8610563](https://t.me/coleccionr)

 ## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">colección Rorikon </span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/" property="cc:attributionName" rel="cc:attributionURL">kAoi97</a>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />
Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.coleccionr.xyz/about/legal" rel="cc:morePermissions">https://www.coleccionr.xyz/about/legal</a>.
  



